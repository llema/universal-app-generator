#!/usr/bin/env node

let shell = require('shelljs')
let colors = require('colors')
let fs = require('fs') //fs already comes included with node.

let templates = require('./templates/templates.js')
const argv = require('yargs').argv

let appDirectory = `${process.cwd()}/${appName}`


const run = async () => {
    let success = await createReactApp()
    if(!success){
      console.log('Something went wrong while trying to create a new Agrofy app. DUMBASS GTFO'.red)
      return false;
    }
    await cdIntoNewApp()
    // await installPackages()
    // await updateTemplates()
    console.log("All done")
  }

    const createReactApp = () => {
    return new Promise(resolve=>{
        if(argv.name){
        shell.exec(`npx create-react-app ${argv.name}`, () => {
            console.log("Created react app")
            resolve(true)
        })
        console.log(argv)
        resolve(true)
        }else{
        console.log("\nNo app name was provided.".red)
        console.log("\nApp name should be an argument to name: ")
        console.log("\ncreate-agrofy-app --name=", "app-name\n".cyan)
            resolve(false)
        }
    })
    }

    const cdIntoNewApp = () => {
    return new Promise(resolve=>{
        shell.exec(`cd ${appName}`, ()=>{resolve()})
    })
    }

    const installPackages = () => {
    return new Promise(resolve=>{
        console.log("\nInstalling redux, react-router, react-router-dom, react-redux, and redux-thunk\n".cyan)
        shell.exec(`npm install --save redux react-router react-redux redux-thunk react-router-dom`, () => {
        console.log("\nFinished installing packages\n".green)
        resolve()
        })
    })
    }

    const updateTemplates = () => {
    return new Promise(resolve=>{
        let promises = []
        Object.keys(templates).forEach((fileName, i)=>{
        promises[i] = new Promise(res=>{
            fs.writeFile(`${appDirectory}/src/${fileName}`, templates[fileName], function(err) {
                if(err) { return console.log(err) }
                res()
            })
        })
        })
        Promise.all(promises).then(()=>{resolve()})
    })
    }

  run() 